<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\MainController@index')->name('main');
Route::get('/catalog', 'App\Http\Controllers\CatalogController@showCatalog')->name('catalog');
Route::get('/etf', 'App\Http\Controllers\CatalogController@showEtf')->name('etf');
Route::get('/stocks', 'App\Http\Controllers\CatalogController@showStocks')->name('stocks');
Route::get('/bonds', 'App\Http\Controllers\CatalogController@showBonds')->name('bonds');
Route::get('/indexes', 'App\Http\Controllers\CatalogController@showIndexes')->name('indexes');
Route::get('/growth-leaders', 'App\Http\Controllers\WhattobuyController@showGrowthLeaders')->name('showGrowthLeaders');
Route::get('/ups-downs', 'App\Http\Controllers\WhattobuyController@showUpsDowns')->name('showUpsDowns');
Route::get('/popular', 'App\Http\Controllers\WhattobuyController@showPopular')->name('showPopular');
Route::get('/recommendations', 'App\Http\Controllers\WhattobuyController@showRecommendations')->name('showRecommendations');
Route::get('/whytoinvest', 'App\Http\Controllers\WheretostartController@whyToInvest')->name('whyToInvest');
Route::get('/education', 'App\Http\Controllers\WheretostartController@education')->name('education');
Route::get('/brokerage-services', 'App\Http\Controllers\ServicesController@brokerageServices')->name('brokerageServices');
Route::get('/news', 'App\Http\Controllers\NewsController@showNews')->name('showNews');
Route::get('/analytics', 'App\Http\Controllers\NewsController@showAnalytics')->name('showAnalytics');
Route::get('/terminal-opportunities', 'App\Http\Controllers\TerminalController@showTerminalOpportunities')->name('showTerminalOpportunities');
Route::get('/terminal-instruction', 'App\Http\Controllers\TerminalController@showTerminalInstruction')->name('showTerminalInstruction');
Route::get('/tariffs', 'App\Http\Controllers\TariffController@showTariffs')->name('showTariffs');
Route::get('/contracts-forms', 'App\Http\Controllers\TariffController@showContractsPage')->name('showContractsPage');
Route::get('/history', 'App\Http\Controllers\CompanyController@showHistoryPage')->name('showHistoryPage');
Route::get('/structure', 'App\Http\Controllers\CompanyController@showStructurePage')->name('showStructurePage');
Route::get('/team', 'App\Http\Controllers\CompanyController@showTeamPage')->name('showTeamPage');
Route::get('/benefits', 'App\Http\Controllers\CompanyController@showBenefitsPage')->name('showBenefitsPage');
Route::get('/reports', 'App\Http\Controllers\CompanyController@showReportsPage')->name('showReportsPage');
Route::get('/bank-details', 'App\Http\Controllers\CompanyController@showBankdetailsPage')->name('showBankdetailsPage');
Route::get('/careers', 'App\Http\Controllers\CompanyController@showCareersPage')->name('showCareersPage');
Route::get('/contacts', 'App\Http\Controllers\CompanyController@showContactsPage')->name('showContactsPage');
Route::get('/login', 'App\Http\Controllers\LoginController@showLoginPage')->name('showLoginPage');
Route::get('/register', 'App\Http\Controllers\LoginController@showRegistrationPage')->name('showRegistrationPage');

