<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Возвращает страницу новостей.
     *
     * @return View
     */
    public function showNews(): View
    {
        return view('news.news');
    }

    /**
     * Возвращает страницу аналитики.
     *
     * @return View
     */
    public function showAnalytics(): View
    {
        return view('news.analytics');
    }
}
