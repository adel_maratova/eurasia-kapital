<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class TerminalController extends Controller
{
    /**
     * Возвращает страницу "Возможности терминала".
     *
     * @return View
     */
    public function showTerminalOpportunities(): View
    {
        return view('terminal.opportunities');
    }

    /**
     * Возвращает страницу "Интсрукция по работе терминала".
     *
     * @return View
     */
    public function showTerminalInstruction(): View
    {
        return view('terminal.instruction');
    }
}
