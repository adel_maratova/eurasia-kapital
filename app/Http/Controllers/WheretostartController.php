<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class WheretostartController extends Controller
{
    /**
     * Возвращает страницу "почему нужно инвестировать".
     *
     * @return View
     */
    public function whyToInvest(): View
    {
        return view('wheretostart.whytoinvest');
    }

    /**
     * Возвращает страницу "обучение".
     *
     * @return View
     */
    public function education(): View
    {
        return view('wheretostart.education');
    }
}
