<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Возвращает страницу логина.
     *
     * @return View
     */
    public function showLoginPage(): View
    {
        return view('login.login');
    }

    /**
     * Возвращает страницу регистрации.
     *
     * @return View
     */
    public function showRegistrationPage(): View
    {
        return view('login.registration');
    }
}
