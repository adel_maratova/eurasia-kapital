<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * Возвращает страницу роста и падений.
     *
     * @return View
     */
    public function brokerageServices(): View
    {
        return view('services.brokerageservices');
    }
}
