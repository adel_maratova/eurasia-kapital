<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class MainController extends Controller
{
    /**
     * Возвращает главную страницу.
     *
     * @return View
     */
    public function index(): View
    {
        return view('main.main');
    }
}
