<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Возвращает страницу истории компании.
     *
     * @return View
     */
    public function showHistoryPage(): View
    {
        return view('company.history');
    }

    /**
     * Возвращает страницу структуры компании.
     *
     * @return View
     */
    public function showStructurePage(): View
    {
        return view('company.structure');
    }

    /**
     * Возвращает страницу команды компании.
     *
     * @return View
     */
    public function showTeamPage(): View
    {
        return view('company.team');
    }

    /**
     * Возвращает страницу преимуществ компании.
     *
     * @return View
     */
    public function showBenefitsPage(): View
    {
        return view('company.benefits');
    }

    /**
     * Возвращает страницу отчетностей и документов компании.
     *
     * @return View
     */
    public function showReportsPage(): View
    {
        return view('company.reports');
    }

    /**
     * Возвращает страницу реквизитов компании.
     *
     * @return View
     */
    public function showBankdetailsPage(): View
    {
        return view('company.bankdetails');
    }

    /**
     * Возвращает страницу вакансий компании.
     *
     * @return View
     */
    public function showCareersPage(): View
    {
        return view('company.careers');
    }

    /**
     * Возвращает страницу контактов компании.
     *
     * @return View
     */
    public function showContactsPage(): View
    {
        return view('contacts');
    }
}
