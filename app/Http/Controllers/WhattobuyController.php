<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class WhattobuyController extends Controller
{
    /**
     * Возвращает страницу лидеров роста.
     *
     * @return View
     */
    public function showGrowthLeaders(): View
    {
        return view('whattobuy.growthleaders');
    }

    /**
     * Возвращает страницу роста и падений.
     *
     * @return View
     */
    public function showUpsDowns(): View
    {
        return view('whattobuy.upsdowns');
    }

    /**
     * Возвращает страницу популярных.
     *
     * @return View
     */
    public function showPopular(): View
    {
        return view('whattobuy.popular');
    }

    /**
     * Возвращает страницу реккомендаций.
     *
     * @return View
     */
    public function showRecommendations(): View
    {
        return view('whattobuy.recommendations');
    }
}
