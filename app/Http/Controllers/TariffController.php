<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class TariffController extends Controller
{
    /**
     * Возвращает страницу "Виды тарифов".
     *
     * @return View
     */
    public function showTariffs(): View
    {
        return view('tariffs.list');
    }

    /**
     * Возвращает страницу "Договор и анкеты".
     *
     * @return View
     */
    public function showContractsPage(): View
    {
        return view('tariffs.contracts');
    }
}
