<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    /**
     * Возвращает страницу каталогов.
     *
     * @return View
     */
    public function showCatalog(): View
    {
        return view('catalog.list');
    }

    /**
     * Возвращает страницу ETF.
     *
     * @return View
     */
    public function showEtf(): View
    {
        return view('catalog.etf');
    }

    /**
     * Возвращает страницу акций.
     *
     * @return View
     */
    public function showStocks(): View
    {
        return view('catalog.stocks');
    }

    /**
     * Возвращает страницу облигаций.
     *
     * @return View
     */
    public function showBonds(): View
    {
        return view('catalog.bonds');
    }

    /**
     * Возвращает страницу индексов.
     *
     * @return View
     */
    public function showIndexes(): View
    {
        return view('catalog.indexes');
    }
}
