@extends('layouts.mainApp')
@section('content')
    <div class="row">
        <div class="open-account background row-full"></div>
        <div class="row open-account content">
            <div class="col-md-6">
                <div class="moto blue1 mt-5">
                    Свободные деньги<br>
                    должны приносить
                    <div class="d-inline-flex">доход<div class="yellow-text ml-2">выше инфляции</div></div>
                </div>

                <div class="benefits">
                    <div class="benefit-bullet">
                        <img src="img/icons/house.png" class="">
                        <div class="ml-2">Открытие счета не выходя из дома</div>
                    </div>
                    <div class="benefit-bullet">
                        <img src="img/icons/deal.png" class="">
                        <div class="ml-2">Сделки на казахстанском и международном рынках</div>
                    </div>
                    <div class="benefit-bullet">
                        <img src="img/icons/support.png" class="">
                        <div class="ml-2">Поддержка консультанта</div>
                    </div>
                </div>
                <button class="btn btn-rounded btn-yellow mt-5">Открыть счет</button>
            </div>
            <div class="col"></div>
        </div>
    </div>
    <div class="why-to-invest">
        <div class="why-to-invest background row-full"></div>
        <div class="why-to-invest content">
            <div class="mt-3" style="height: 30px;"></div>
            <div class="mt-5 category-title d-flex justify-content-center">Почему нужно инвестировать</div>
            <div class="mt-3 small-title d-flex justify-content-center o-90p">Или чем инвестиции выгоднее вклада</div>
            <div class="row d-flex justify-content-between mt-5">
                <div class="white-shadow-box">
                    <b>Доходность по депозитам</b> <br>
                    с 01.04.2020 по 01.04.2021 составила
                </div>
                <div class="round">10-12%</div>
                <div class="double-arrows"><img src="img/icons/next.png"><img src="img/icons/next.png"></div>
                <div class="d-flex justify-content-end">
                    <div class="white-shadow-box ml-5 ">
                    <b>Доходность по депозитам (индекс KASE)</b> <br>
                    с 01.04.2020 по 01.04.2021 выросли на
                </div>
                <div class="round yellow-gradient-round" style="padding-top: 25px; padding-left: 20px"><div class="d-flex align-items-center">39<div style="font-size:20px " class="ml-1">%</div></div></div>
                </div>

            </div>
            <div class="row d-flex justify-content-end mt-5 blue1 link-more-details">Подробнее</div>
            <div class="mt-2" style="height: 30px;"></div>
            <div class="mt-5 category-title d-flex justify-content-center">Какие существуют возможности?</div>
            <div class="row mt-5">
                <div class="col white-shadow-box opportunities">
                    <div class="par-title">Акции</div>
                    <div>Ценная бумага, свидетельствующая о том, что вы являетесь совладельцем компании и имеете право участвовать в прибыли компании. Владелец акций может получать прибыль от роста стоимости акций и плюс от дивидендов компании.</div>
                    <div class="d-flex justify-content-end" style="margin-top:-20px "><img class=""  src="img/icons/tg-growth.png"></div>
                </div>
                <div class="col white-shadow-box opportunities ml-5">
                    <div class="par-title">Облигации</div>
                    <div>Владелец облигаций может получать прибыль в виде фиксированных процентов (получать купон) и может получать прибыль от роста стоимости облигации.</div>
                    <div class="d-flex justify-content-end mt-4"><img src="img/icons/tg-cert.png"></div>
                </div>
                <div class=" col white-shadow-box opportunities ml-5">
                    <div class="par-title">ETF</div>
                    <div>ETF или инвестиционный фонд -
                        это портфель разных ценных бумаг, собранный командой профессионалов. Суть его сбалансировать доходы и риски ценных бумаг.</div>
                    <div class="d-flex justify-content-end"><img src="img/icons/tg-flower.png"></div>
                </div>
            </div>



        </div>
    </div>
    <div class="{{--why-to-invest--}} invest-from-home" {{--style="height: 1212px;"--}}>
        {{--<div class="mt-5"></div>--}}
        <div class="{{--why-to-invest--}} invest-from-home content mt-5">
            <div class="{{--mt-3--}}" style="height: 20px;"></div>
            <div class="category-title d-flex justify-content-center">Инвестируйте, не выходя из дома</div>
            <div class="row mt-5">
                <div class="col white-shadow-box opportunities">
                    <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                    <div class="company-title">Казахтелеком</div>
                    <div class="card-legend mt-2">27 500 ₸</div>
                    <div class="d-inline-flex align-items-center mt-3">
                        <img src="img/icons/up.png" class="arrow-up">
                        <div class="card-legend ml-2">34%</div>
                        <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                        <div class="gray-info-icon ml-3"></div>
                    </div>
                    <button class="yellow-border-btn mt-3">Купить</button>
                </div>
                <div class="col white-shadow-box opportunities ml-5">
                    <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                    <div class="company-title">Казахтелеком</div>
                    <div class="card-legend mt-2">27 500 ₸</div>
                    <div class="d-inline-flex align-items-center mt-3">
                        <img src="img/icons/up.png" class="arrow-up">
                        <div class="card-legend ml-2">34%</div>
                        <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                        <div class="gray-info-icon ml-3"></div>
                    </div>
                    <button class="yellow-border-btn mt-3">Купить</button>
                </div>
                <div class=" col white-shadow-box opportunities ml-5">
                    <div class="d-flex justify-content-between"><div class="item-title">ETF</div><div class="kaz-tel" ></div></div>
                    <div class="company-title">FXKZ ETF</div>
                    <div class="card-legend mt-2">27 500 ₸</div>
                    <div class="d-inline-flex align-items-center mt-3">
                        <img src="img/icons/up.png" class="arrow-up">
                        <div class="card-legend ml-2">34%</div>
                        <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                        <div class="gray-info-icon ml-3"></div>
                    </div>
                    <button class="yellow-border-btn mt-3">Купить</button>
                </div>
                <div class=" col white-shadow-box opportunities ml-5">
                    <div class="d-flex justify-content-between"><div class="item-title">ОБЛИГАЦИИ</div><div class="es"></div></div>
                    <div class="company-title">Электроцит-
                        Стройсистема выпуск 2</div>
                    <div class="card-legend mt-2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ">27 500 ₸</div>
                    <div class="d-inline-flex align-items-center mt-3">
                        <img src="img/icons/up.png" class="arrow-up">
                        <div class="card-legend ml-2">34%</div>
                        <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                        <div class="gray-info-icon ml-3"></div>
                    </div>
                    <button class="yellow-border-btn mt-3">Купить</button>
                </div>
            </div>
            <div class="d-flex justify-content-center mt-5">
                <button class="btn btn-rounded btn-yellow">Перейти в каталог</button>
            </div>
            <div class="" style="height: 50px;"></div>
            <div class="light-blue tbl-key-indicators mt-5 mb-2 ">
                <div class="d-flex justify-content-between p-2 ">
                    <div class="blue-14">Ключевые показатели</div>
                    <div class="light-gray-12">Торги на бирже KASE открываются через 3 ч 39 мин</div>
                </div>
                <div class="row p-2">
                    <div class="col white-border-right">
                        <div class="black-16 thin-white-underline p-1">S&P 500</div>
                        <div class="black-16 font-weight-bold p-1">4077.91</div>
                        <div class="d-flex justify-content-between black-12">+58.040 (+1.44%) <img src="img/icons/up.png" class="arrow-up"></div>
                    </div>
                    <div class="col">
                        <div class="black-16 thin-white-underline p-1">Nasdaq</div>
                        <div class="black-16 font-weight-bold p-1">6737,91</div>
                        <div class="d-flex justify-content-between black-12">+23,670 (+0,35%)<img src="img/icons/up.png" class="arrow-up"></div>
                    </div>
                    <div class="col">
                        <div class="black-16 thin-white-underline p-1">FTSE 100</div>
                        <div class="black-16 font-weight-bold p-1">6737,91</div>
                        <div class="d-flex justify-content-between black-12">-23,670 (-0,35%)<img src="img/icons/red-down.png" class="arrow-up"></div>
                    </div>
                    <div class="col">
                        <div class="black-16 thin-white-underline p-1">Nasdaq</div>
                        <div class="black-16 font-weight-bold p-1">6737,91</div>
                        <div class="d-flex justify-content-between black-12">+23,670 (+0,35%)<img src="img/icons/up.png" class="arrow-up"></div>
                    </div>
                    <div class="col">
                        <div class="black-16 thin-white-underline p-1">FTSE 100</div>
                        <div class="black-16 font-weight-bold p-1">6737,91</div>
                        <div class="d-flex justify-content-between black-12">-23,670 (-0,35%)<img src="img/icons/red-down.png" class="arrow-up"></div>
                    </div>
                    <div class="col">
                        <div class="black-16 thin-white-underline p-1">FTSE 100</div>
                        <div class="black-16 font-weight-bold p-1">6737,91</div>
                        <div class="d-flex justify-content-between black-12">-23,670 (-0,35%)<img src="img/icons/red-down.png" class="arrow-up"></div>
                    </div>
                </div>
            </div>
            <div class="mt-11 category-title d-flex justify-content-center">Станьте инвестором всего за четыре шага</div>
            <div class="row mt-5 text-center">
                <div class="col">
                    <div class="step-1"></div>
                    <div class="blue-16 p-20 lh-20 h-80" >Подайте заявку</div>
                    <div class="fw-normal">Вы можете открыть счет самостотельно онлайн. Никуда не нужно ехать</div>
                </div>
                <div class="col ml-5">
                    <div class="step-2"></div>
                    <div class="blue-16 p-20 lh-20 h-80">Откройте счет онлайн с помощью ЭЦП*</div>
                    <div class="fw-normal">Заполните оставшиеся данные. Это займет всего несколько минут.</div>
                </div>
                <div class="col ml-5">
                    <div class="step-3"></div>
                    <div class="blue-16 p-20 lh-20 h-80">Пополните счет</div>
                    <div class="fw-normal">Удобные варианты для пополнения на выбор</div>
                </div>
                <div class="col ml-5 ">
                    <div class="step-4"></div>
                    <div class="blue-16 p-20 lh-20 h-80">Купите ценные бумаги</div>
                    <div class="fw-normal">Покупайте и продавайте онлайн самостоятельно. Или воспользуетесь помощью наших брокеров</div>
                </div>
            </div>
            <div class="d-flex justify-content-center mt-3 note">*При отсутствие ЭЦП вы сможете выслать нам свои данные. Наши специалисты заполнят все документы и пришлют Вам на подпись</div>
        </div>
    </div>
    <div class="enter-your-details row-full d-flex justify-content-center light-blue">
        <div class="">
            <div class="small-title mt-5 d-flex justify-content-center">Введите свои данные (шаг 1 из 4)</div>
            <div><input class="mt-5 details-txt-input" type="text" placeholder="Фамилия, имя и отчество"></div>
            <div><input class="mt-3 details-txt-input" type="text" placeholder="Мобильный телефон"></div>
            <div class="d-flex justify-content-center">
                <button class="mt-4 btn btn-rounded btn-yellow d-flex justify-content-center">Открыть счет</button>
            </div>
        </div>
    </div>
    <div class="comfort-investing container">
        <div class="category-title d-flex justify-content-center mt-5">Комфортное инвестирование</div>
        <div class="comfort-investing content mt-4">
            <div class="row">
                <div class="col-8 white-shadow-box comfort-investing-main-point mr-5 deal-bg-ci">
                    <div class="small-title mt-3">Сделки на международных и казахстанских рынках</div>
                    <div class="mt-2 comfort-investing-p">
                        На казахстанском рынке сделки с ценными бумагами проходят как на бирже KASE (Казахстанская Фондовая Биржа), на бирже AIX (МФЦА) так и вне биржи.
                        <br>
                        <br>
                        Также мы предоставляем инвесторам выход на международные биржи Лондона, Нью-Йорка и другие. Выбор ценных бумаг свыше 11000. Поэтому здесь будет полезной консультация наших специалистов.
                    </div>
                    <div class="d-flex justify-content-between align-items-center mt-8">
                        <div class="blue1 link-more-details ">Подробнее</div>
                        <img src="{{asset('img/icons/deal-percent.png')}}">
                    </div>
                </div>
                <div class="col white-shadow-box comfort-investing-main-point">
                    <div class="small-title mt-3">Онлайн-советник</div>
                    <p class="mt-2 comfort-investing-p">
                        Соберет сбалансированный портфель, задав всего несколько вопросов
                    </p>
                    <div class="d-flex justify-content-between align-items-center mt-13">
                        <div class="blue1 link-more-details">Получить совет</div>
                        <img src="{{asset('img/icons/consultant.png')}}">
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col white-shadow-box comfort-investing-point" >
                    <div class="small-title mt-3">Услуги управления Вашими активами</div>
                    <p class="mt-2 comfort-investing-p">
                        Располагая суммой свыше 30 000 долларов США (или ее эквивалентом в тенге) вы можете стать пользователем такой услуги как «Доверительное Управление»
                    </p>
                    <div class="d-flex justify-content-between align-items-center mt-5-5">
                        <div class="blue1 link-more-details">Подробнее</div>
                        <img src="{{asset('img/icons/handshake.png')}}">
                    </div>
                </div>
                <div class="col white-shadow-box comfort-investing-point ml-5">
                    <div class="small-title mt-3">ETF</div>
                    <p class="mt-2 comfort-investing-p">
                        Мы являемся экспертами в сфере инвестирования в ETF. Ознакомьтесь с нашей подборкой ETF и нашими рекомендациями
                    </p>
                    <div class="d-flex justify-content-between align-items-center mt-7">
                        <div class="blue1 link-more-details">Подробнее</div>
                        <img src="{{asset('img/icons/etf.png')}}">
                    </div>
                </div>
                <div class="col white-shadow-box comfort-investing-point ml-5">
                    <div class="small-title mt-3">Терминал</div>
                    <p class="mt-2 comfort-investing-p">
                        Удобный инструмент для инвестора. Терминал позволяет онлайн покупать и продавать ЦБ, видеть текущие котировки, переводить деньги, подавать другие приказы и заявки
                    </p>
                    <div class="d-flex justify-content-between align-items-center mt-2">
                        <div class="blue1 link-more-details">Подробнее</div>
                        <img src="{{asset('img/icons/terminal.png')}}">
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col white-shadow-box comfort-investing-point">
                    <div class="small-title mt-3">Юридическим лицам</div>
                    <p class="mt-2 comfort-investing-p">
                        Комплекс услуг по инвестированию в тенге и в долларах, по финансированию Вашего бизнеса, включающий организацию выпусков, размещение долговых и долевых ценных бумаг.                    </p>
                    <div class="d-flex justify-content-between align-items-center mt-2">
                        <div class="blue1 link-more-details">Подробнее</div>
                        <img src="{{asset('img/icons/paper-pen.png')}}">
                    </div>
                </div>
                <div class="col white-shadow-box comfort-investing-point ml-5">
                    <div class="small-title mt-3">Клуб инвесторов</div>
                    <p class="mt-2 comfort-investing-p">
                        Будьте всегда в курсе и на связи.
                        Самые актуальные новости и подборки в нашем телеграм-канале для инвесторов
                        АО “Евразийский Капитал”
                    </p>
                    <div class="d-flex justify-content-between align-items-center mt-4-5">
                        <div class="blue1 link-more-details">Присоединиться</div>
                        <img src="{{asset('img/icons/investors.png')}}">
                    </div>
                </div>
                <div class="col white-shadow-box comfort-investing-point ml-5">
                    <div class="small-title mt-3">С чего начать?</div>
                    <p class="mt-2 comfort-investing-p">
                        Инвестировать может каждый. Для этого необходимо лишь изучить основы и научиться пользоваться инструментами. Сделайте первые шаги с помощью нашего обучения. Инвестируйте в себя!
                    </p>
                    <div class="d-flex justify-content-between align-items-center mt-3">
                        <div class="blue1 link-more-details">Начать обучение</div>
                        <img src="{{asset('img/icons/rupor.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mp-blocks-section">
        <div class="mp-blocks-section background row-full"></div>
        <div class="mp-blocks-section content">
            <div class="row">
                <div class="col">
                    <div class="category-title mt-5">Инвест-идеи</div>
                    <div class="white-shadow-box mt-3 invest-ideas-box">
                        <div class="piece-of-news">
                            <div class="news-block-title mt-2">Обзор перед IPO ZoomInfo Technologies (ZI): платформа для сокращения цикла продаж</div>
                            <div class="news-block-description mt-3">
                                Можно заработать: <br>
                                До 61%  за 11 месяцев
                            </div>
                            <div class="news-block-date-source mt-2">
                                9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                            </div>
                        </div>
                        <div class="piece-of-news mt-3-5">
                            <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                            <div class="news-block-date-source mt-3">
                                9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                            </div>
                        </div>
                        <div class="piece-of-news mt-3-5">
                            <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI)</div>
                            <div class="news-block-date-source mt-3">
                                9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                            </div>
                        </div>
                        <div class="piece-of-news mt-3-5">
                            <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                            <div class="news-block-date-source mt-3">
                                9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                            </div>
                        </div>
                        <div class="blue1 link-more-details mt-10">Подробнее</div>
                    </div>
                </div>
                <div class="col">
                    <div class="category-title mt-5">Аналитика</div>
                    <div class="white-shadow-box analytics-box mt-3">
                        <div class="piece-of-news mt-3-5">
                            <div class="news-block-title mt-2">Apple сокращает планы по производству iPhone 12 mini</div>
                            <div class="news-block-date-source mt-3">
                                9 марта 2021
                            </div>
                        </div>
                        <div class="piece-of-news mt-3-5">
                            <div class="news-block-title mt-2">Фондовые индексы США растут в начале торгов</div>
                            <div class="news-block-date-source mt-3">
                                11 Марта 2021г
                            </div>
                        </div>
                        <div class="blue1 link-more-details mt-4">Подробнее</div>
                    </div>
                    <div class="category-title mt-6">Новости</div>
                    <div class="white-shadow-box news-box mt-2">
                        <div class="piece-of-news mt-3-5">
                            <div class="news-block-title mt-2">Еженедельный обзор (1 - 5 марта 2021 года)</div>
                            <div class="news-block-date-source mt-3">
                                9 марта 2021
                            </div>
                        </div>
                        <div class="piece-of-news mt-3-5">
                            <div class="news-block-title mt-2">Льготное кредитование для бизнеса</div>
                            <div class="news-block-date-source mt-3">
                                11 Марта 2021г
                            </div>
                        </div>
                        <div class="blue1 link-more-details mt-5">Подробнее</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mp-company-info">
        <div class="row">
            <div class="col mt-5">
                <div class=" category-title">АО “Евразийский капитал”</div>
                <div class="mt-2 comfort-investing-p">
                    является профессиональным участником рынка ценных бумаг и обладает лицензиями уполномоченного органа на оказание брокерской и дилерской деятельности с правом ведения счетов клиентов и на управление инвестиционным портфелем.
                </div>
                <div class="blue1 link-more-details mt-12">Подробнее</div>
            </div>
            <div class="col mt-5">
                <div class="company-benefit">
                    <div class="mp-company-info-title">
                        Доступ к торгам с любого устройства
                    </div>
                    <div class="mt-2 comfort-investing-p">
                        Торговый терминал работает на мобильных устройствах с Android, IOS, и с ноутбуков и стационарных компьютеров.
                    </div>
                </div>
                <div class="company-benefit mt-4">
                    <div class="mp-company-info-title">
                        Уникальные продукты
                    </div>
                    <div class="mt-2 comfort-investing-p">
                        Исходя из срока инвестирования и суммы инвестирования подбираем нужные акции или ETF как в Казахстане так и на других биржах мира.
                    </div>
                </div>
                <div class="company-benefit mt-4">
                    <div class="mp-company-info-title">
                        Доступный листинг
                    </div>
                    <div class="mt-2 comfort-investing-p">
                        Выводим на листинг акции и облигации быстро и наши комиссии ниже чем в среднем по рынку.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mp-illustration">
        <div class="mp-illustration background row-full"></div>
    </div>
    <div class="questions-left light-blue row-full d-flex justify-content-center">
        <div class="">
            <div class="category-title mt-6 d-flex justify-content-center">Остались вопросы?</div>
            <div class="black-16 d-flex justify-content-center font-weight-bolder mt-2">Отправьте заявку</div>
            <div class="fw-400 black-16 d-flex justify-content-center mt-3">Наш брокер свяжется с Вами и ответит на все вопросы</div>
            <div><input class="mt-5 details-txt-input" type="text" placeholder="Имя"></div>
            <div><input class="mt-3 details-txt-input" type="text" placeholder="Мобильный телефон"></div>
            <div class="d-flex justify-content-center">
                <button class="mt-6 btn btn-rounded btn-yellow d-flex justify-content-center">Отправить заявку</button>
            </div>
            <div class="mt-6 d-flex justify-content-center">Или напишите нам на WhatsApp  <img src="img/contact/whatsapp-logo.png" class="mr-2 ml-2"> +7 (777) 033 19 76</div>
        </div>
    </div>
@endsection
