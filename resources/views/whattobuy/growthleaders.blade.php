@extends('layouts.mainApp')
@section('content')
    <div class="page-title mt-5" id="growth-leaders">Лидеры роста</div>
    <div class="d-flex flex-row mt-4 mb-5">
        <div class="white-shadow-box growth-leaders-box tenge-box">
            <div class="title">Тенге</div>
            <div class="mt-4 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
        <div class="white-shadow-box ml-4 growth-leaders-box dollars-box">
            <div class="title">Доллары</div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/apple.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Apple</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
        <div class="white-shadow-box ml-4 growth-leaders-box etf-box">
            <div class="title">Топ-10 ETF</div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/apple.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Apple</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
    </div>
    <div class="page-title mt-5" id="ups-downs">Взлеты и падения</div>
    <div class="d-flex flex-row mt-4 mb-5">
        <div class="white-shadow-box growth-leaders-box tenge-box col-sm-5">
            <div class="title">Топ 10</div>
            <div class="box-blue-title mt-2">Взлеты дня</div>
            <div class="mt-4 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
        <div class="white-shadow-box ml-4 growth-leaders-box dollars-box col-sm-5">
            <div class="title">Топ 10</div>
            <div class="box-blue-title mt-2">Падения дня</div>
            <div class="mt-4 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
    </div>
    <div class="page-title mt-5" id="popular">Популярные инструменты на Ecap</div>
    <div class="d-flex flex-row mt-4 mb-5">
        <div class="white-shadow-box growth-leaders-box tenge-box col-sm-5">
            <div class="title">ETF</div>
            <div class="box-blue-title mt-2">Топ просмотров</div>
            <div class="mt-4 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
        <div class="white-shadow-box ml-4 growth-leaders-box dollars-box col-sm-5">
            <div class="title">АКЦИИ</div>
            <div class="box-blue-title mt-2">Что ищут</div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/apple.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Apple</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
    </div>
    <div class="page-title mt-5 mb-4">Рекомендации</div>
    <div class="d-flex flex-row mt-4 mb-5">
        <div class="white-shadow-box growth-leaders-box tenge-box">
            <div class="box-blue-title mt-2">Bloomberg</div>
            <div class="mt-4 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
        <div class="white-shadow-box ml-4 growth-leaders-box dollars-box">
            <div class="box-blue-title mt-2" id="recommendations">Рекомендации экспертов</div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/apple.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Apple</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
        <div class="white-shadow-box ml-4 growth-leaders-box etf-box">
            <div class="box-blue-title mt-2">Bloomberg</div>
            <div class="mt-3 d-flex flex-row">
                <div class="kazahtelecom-logo"></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Казахтелеком</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-4 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/apple.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Apple</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/up.png')}}" alt="" class="arrow-up">
                        <div class="growth-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="mt-3 d-flex flex-row">
                <div class="company-back-logo d-flex justify-content-center align-items-center"><img src="{{asset('img/comp-logos/tesla.png')}}" alt=""></div>
                <div class="d-flex flex-column ml-3 justify-content-center">
                    <div class="company">Tesla</div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('img/icons/down.png')}}" alt="" class="arrow-up">
                        <div class="fall-info ml-2"> 0,27 ₸ (0,24%)</div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details mt-4 mb-5">Смотреть все ></div>
        </div>
    </div>
@endsection
