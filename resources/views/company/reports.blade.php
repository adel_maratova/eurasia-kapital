@extends('layouts.mainApp')
@section('content')
    <div class="page-title mt-5">Отчетность</div>
    <div class="education-tabs-section mt-4">
        <div class="tab">
            <button class="tablinks news-tab" onclick="openTab(event, 'financial-reports')" id="defaultOpen">Финансовая отчетность</button>
            <button class="tablinks news-tab" onclick="openTab(event, 'pdo-reports')">Отчет ПДО</button>
        </div>
        <!-- Tab content -->
        <div id="financial-reports" class="tabcontent">
            <table id="reports-table" class="table table-reports">
                <thead>
                <tr>
                    <th class="">Сортировка по алфавиту</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="literature-title">Финансовая отчетность АО “Евразийский Капитал” за 4 квартал 2020 года</div>
                            <div class="news-block-date-source mt-2">Размещено 19 января 2021 года</div>
                        </div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="blue1 link-more-details d-flex justify-content-center ">Показать еще</div>
        </div>

        <div id="pdo-reports" class="tabcontent">
            <div class="mt-3">
                <div class="small-title">Льготное кредитование для бизнеса</div>
                <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                    обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                    дальнейших направлений развития...
                </div>
                <div class="news-block-date-source mt-3">
                    9 марта 2021
                </div>
            </div>
            <div class="mt-7">
                <div class="small-title">Льготное кредитование для бизнеса</div>
                <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                    обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                    дальнейших направлений развития...
                </div>
                <div class="news-block-date-source mt-3">
                    9 марта 2021
                </div>
            </div>
            <div class="mt-7">
                <div class="small-title">Льготное кредитование для бизнеса</div>
                <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                    обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                    дальнейших направлений развития...
                </div>
                <div class="news-block-date-source mt-3">
                    9 марта 2021
                </div>
            </div>
            <div class="mt-7">
                <div class="small-title">Льготное кредитование для бизнеса</div>
                <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                    обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                    дальнейших направлений развития...
                </div>
                <div class="news-block-date-source mt-3">
                    9 марта 2021
                </div>
            </div>
            <div class="blue1 link-more-details d-flex justify-content-center mt-9">Показать еще</div>
        </div>
    </div>
    <div class="documents-section">
        <div class="page-title mt-8">Документы</div>
        <div class="d-flex flex-row mt-5">
            <div class="col p-0 text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Устав АО “Евразийский капитал”</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Справка о гос. регистрации</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Лицензия НБ РК</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Лицензия другая</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Лицензия другая длинное название</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Реквизиты компании</div>
            </div>
        </div>
        <div class="d-flex flex-row mt-8">
            <div class="col p-0 text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Устав АО “Евразийский капитал”</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Справка о гос. регистрации</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Лицензия НБ РК</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Лицензия другая</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Лицензия другая длинное название</div>
            </div>
            <div class="col text-center">
                <img src="{{asset('img/miniature/document.png')}}" alt="">
                <div class="document-title">Реквизиты компании</div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready (function(e){
            document.getElementById("defaultOpen").click();
        })

        function openTab(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        $(document).ready(function() {
            $('#reports-table').DataTable({
                'searching': false,
                'ordering': true,
                "lengthChange": false,
                'info':false,
                'pagingType':'full_numbers',
                'language': {
                    'paginate': {
                        'first': '<<',
                        'previous': '<',
                        'next': '>',
                        'last': '>>',
                    }
                },
                'stripped':false,
                'pageLength' : 5,
            });
        } );
    </script>
@endsection
