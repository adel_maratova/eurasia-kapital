@extends('layouts.mainApp')
@section('content')
    <div class="careers-page">
        <div class="page-title mt-4">Вакансии</div>
        <div class="d-flex flex-row">
            <div class="col-sm p-0">
                <div class="white-shadow-box vacancy-box mt-3">
                    <div class="vacancy-title">PYTHON DEVELOPER</div>
                    <div class="mt-4 requrements"><b>Требования</b></div>
                    <div class="black-16 mt-1 lh-30">
                        Уверенное знание Django framework;<br>
                        Знание SQL (PostgreSQL), Redis, Docker, Celery + RabbitMQ, Linux;<br>
                        Git;<br>
                        MVC;<br>
                        ООП;<br>
                        REST/SOAP;<br>
                        Умение разбираться в чужом коде;<br>
                        Умение составлять сложные SQL-запросы.<br>
                    </div>
                    <button class="mt-5 yellow-border-btn btn-respond">Откликнуться</button>
                </div>
                <div class="white-shadow-box vacancy-box mt-5">
                    <div class="vacancy-title">PYTHON DEVELOPER</div>
                    <div class="mt-4 requrements"><b>Требования</b></div>
                    <div class="black-16 mt-1 lh-30">
                        Уверенное знание Django framework;<br>
                        Знание SQL (PostgreSQL), Redis, Docker, Celery + RabbitMQ, Linux;<br>
                        Git;<br>
                        MVC;<br>
                        ООП;<br>
                        REST/SOAP;<br>
                        Умение разбираться в чужом коде;<br>
                        Умение составлять сложные SQL-запросы.<br>
                    </div>
                    <button class="mt-5 yellow-border-btn btn-respond">Откликнуться</button>
                </div>
            </div>
            <div class="col-sm ml-5">
                <div class="send-resume-box">
                    <div class="d-flex flex-row">
                        <div class="col-sm-3  mt-4">
                            <img src="{{asset('img/icons/cv.png')}}" alt="" class="cv-pic mt-4">
                        </div>
                        <div class="col-sm-9  mt-4">
                            <div class="vacancy-title">Присылайте свои резюме</div>
                            <div class="mt-3 black-16">
                                Мы всегда рады профессионалам своего дела. Если вы хотите работать в нашей команде, отправьте резюме на
                            </div>
                            <a href="#" class="light-blue-link mt-2"><u>info@ecap.kz</u></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
