@extends('layouts.mainApp')
@section('content')
    <div class="staff">
        <div class="page-title mt-5" id="growth-leaders">Команда</div>
        <div class="par-title mt-4">Руководство</div>
        <div class="d-flex flex-row mt-3">
            <div class="col-sm p-0 d-flex flex-column text-center" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/askar_aitkozha.png')}}"></div>
                <div class="staff-name mt-2">Асқар Айтқожа</div>
                <div class="staff-position mt-2">Председатель Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/gulnur-omarhanova.png')}}"></div>
                <div class="staff-name mt-2">Гульнур Омарханова</div>
                <div class="staff-position mt-2">Начальник Департамента<br>
                    управления инвестиционным<br> портфелем, Член Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/asan-sadvakasov.png')}}"></div>
                <div class="staff-name mt-2">Асан Садвакасов</div>
                <div class="staff-position mt-2">Заместитель Председателя Правления,<br> Член Правления</div>
            </div>
        </div>
        <div class="par-title mt-9">Правление</div>
        <div class="d-flex flex-row mt-3">
            <div class="col-sm p-0 d-flex flex-column text-center" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/askar_aitkozha.png')}}"></div>
                <div class="staff-name mt-2">Асқар Айтқожа</div>
                <div class="staff-position mt-2">Председатель Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/gulnur-omarhanova.png')}}"></div>
                <div class="staff-name mt-2">Гульнур Омарханова</div>
                <div class="staff-position mt-2">Начальник Департамента<br>
                    управления инвестиционным<br> портфелем, Член Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/asan-sadvakasov.png')}}"></div>
                <div class="staff-name mt-2">Асан Садвакасов</div>
                <div class="staff-position mt-2">Заместитель Председателя Правления,<br> Член Правления</div>
            </div>
        </div>
        <div class="par-title mt-9">Команда</div>
        <div class="d-flex flex-row mt-3">
            <div class="col-sm p-0 d-flex flex-column text-center" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/askar_aitkozha.png')}}"></div>
                <div class="staff-name mt-2">Асқар Айтқожа</div>
                <div class="staff-position mt-2">Председатель Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/gulnur-omarhanova.png')}}"></div>
                <div class="staff-name mt-2">Гульнур Омарханова</div>
                <div class="staff-position mt-2">Начальник Департамента<br>
                    управления инвестиционным<br> портфелем, Член Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/asan-sadvakasov.png')}}"></div>
                <div class="staff-name mt-2">Асан Садвакасов</div>
                <div class="staff-position mt-2">Заместитель Председателя Правления,<br> Член Правления</div>
            </div>
        </div>
        <div class="d-flex flex-row mt-5">
            <div class="col-sm p-0 d-flex flex-column text-center" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/askar_aitkozha.png')}}"></div>
                <div class="staff-name mt-2">Асқар Айтқожа</div>
                <div class="staff-position mt-2">Председатель Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/gulnur-omarhanova.png')}}"></div>
                <div class="staff-name mt-2">Гульнур Омарханова</div>
                <div class="staff-position mt-2">Начальник Департамента<br>
                    управления инвестиционным<br> портфелем, Член Правления</div>
            </div>
            <div class="col-sm p-0 d-flex flex-column text-center ml-3" style="width: 268px">
                <div class="d-flex justify-content-center"><img class="staff-photo" src="{{asset('img/photos/asan-sadvakasov.png')}}"></div>
                <div class="staff-name mt-2">Асан Садвакасов</div>
                <div class="staff-position mt-2">Заместитель Председателя Правления,<br> Член Правления</div>
            </div>
        </div>
    </div>
@endsection
