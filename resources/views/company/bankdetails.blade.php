@extends('layouts.mainApp')
@section('content')
    <div class="bank-details-page">
        <div class="page-title mt-4">Реквизиты</div>
        <div class="bank-details-title mt-4">Консолидированный клиентский счет в АО «Центральный Депозитарий Ценных Бумаг» (KZT)</div>
        <div class="d-flex flex-column col-sm-7 p-0 mt-2">

            <div class="bank-detail-info">
                Бенефициар	АО «Евразийский Капитал»
            </div>
            <div class="bank-detail-info">
                БИН	041040001463
            </div>
            <div class="bank-detail-info">
                Банк бенефициара АО «Центральный Депозитарий Ценных Бумаг»
            </div>
            <div class="bank-detail-info">
                ИИК	KZ067660000262161001
            </div>
            <div class="bank-detail-info">
                БИК	CEDUKZKA
            </div>
            <div class="bank-detail-info">
                КБе	15
            </div>
            <div class="bank-details-title mt-6">АО «Народный Банк Казахстана»</div>
            <div class="bank-detail-info">
                Консолидированный клиентский счет в KZT:
            </div>
            <div class="bank-detail-info">
                Консолидированный клиентский счет в USD
            </div>
            <div class="bank-details-title mt-6">АО «Банк ЦентрКредит»</div>
            <div class="bank-detail-info">
                Консолидированный клиентский счет в KZT:
            </div>
            <div class="bank-detail-info">
                Консолидированный клиентский счет в USD
            </div>
        </div>
    </div>
@endsection
