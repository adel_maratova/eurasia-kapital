@extends('layouts.mainApp')
@section('content')
    <div class="open-account-request-form row-full d-flex justify-content-center light-blue">
        <div class="">
            <div class="page-title mt-4 d-flex justify-content-sm-center">
                Кабинет клиента
            </div>
            <div class="small-title mt-2 d-flex justify-content-center text-center">
                Для входа в личный кабинет необходимо <br>
                авторизоваться
            </div>
            <div>
                <input class="mt-5 details-txt-input" type="text" placeholder="Email">
            </div>
            <div>
                <input class="mt-3 details-txt-input" type="text" placeholder="Пароль">
            </div>
            <div class="d-flex justify-content-md-between mt-4">
                <a href="/register" class="blue1 link-more-details">Зарегистрироваться</a>
                <div class="blue1 link-more-details">Забыли пароль</div>
            </div>
            <div class="d-flex justify-content-center">
                <button class="mt-4 btn btn-rounded btn-yellow d-flex justify-content-center">Войти</button>
            </div>
            <div class="text-center rights-reserved mt-4">
                Не сообщайте логин, пароль и код подтверждения, чтобы этим не <br>
                воспользовались мошенники. Сотрудник банка никогда их не спросит.
            </div>
        </div>
    </div>
@endsection
