@extends('layouts.mainApp')
@section('content')
    <div class=" registration-page row-full d-flex justify-content-center light-blue">
        <div class="container">
            <div class="page-title mt-4 d-flex justify-content-sm-center">
                Открытие счета
            </div>
            <div class="small-title mt-2 d-flex justify-content-center text-center">
                Прежде, чем приступить к открытию счета,<br>
                пожалуйста, прочтите инструкцию
            </div>
            <div class="registration-tabs-section mt-4">
                <div class="tab-register d-flex flex-row">
                    <button class="register_tablinks col" onclick="openTab(event, 'registration-step-1')" id="defaultOpen">Шаг 1: Ввод данных</button>
                    <button class="register_tablinks col" onclick="openTab(event, 'registration-step-2')">Шаг 2: Прикрепление файлов</button>
                    <button class="register_tablinks col" onclick="openTab(event, 'registration-step-3')">Шаг 3: Подписание</button>
                    <button class="register_tablinks col" onclick="openTab(event, 'registration-step-4')">Шаг 4: Отправка данных</button>
                </div>
                <!-- Tab content -->
                <div id="registration-step-1" class="tabcontent registration-tab">
                    <div class="mt-2 par-title d-flex justify-content-sm-center o-90p">Ввод данных для открытия счета </div>
                    <form class="mt-3 registration-form">
                        <div class="form-group row">
                            <label for="iin" class="col-sm-2 col-form-label required d-flex justify-content-sm-end">ИИН*</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" id="iin">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fio" class="col-sm-2 col-form-label required d-flex justify-content-sm-end">ФИО*</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="fio">
                            </div>
                        </div>
                        <div class="form-group row d-flex flex-row align-items-center">
                            <label for="citizenship" class="col-sm-2 col-form-label required d-flex justify-content-sm-end">Гражданство*</label>
                            <div class="col-sm-3">
                                <div class="d-flex flex-row align-items-center">
                                    <input type="radio" class="registration-radio" id="test1" name="radio-group" checked>
                                    <label for="test1" class="no-mb-label ml-3">Гражданин РК</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="d-flex flex-row align-items-center">
                                    <input type="radio" class="registration-radio" id="test2" name="radio-group">
                                    <label for="test2" class="no-mb-label ml-3">Иностранный гражданин</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="d-flex flex-row align-items-center">
                                    <input type="radio" class="registration-radio" id="test3" name="radio-group">
                                    <label for="test3" class="no-mb-label ml-3">Лицо без гражданства</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row d-flex flex-row align-items-center">
                            <label for="citizenship" class="col-sm-2 col-form-label required d-flex justify-content-sm-end text-right">Статус резиденства*</label>
                            <div class="col-sm-3">
                                <div class="d-flex flex-row align-items-center">
                                    <input type="radio" class="registration-radio" id="test1" name="radio-group" checked>
                                    <label for="test1" class="no-mb-label ml-3">Резидент РК</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="d-flex flex-row align-items-center">
                                    <input type="radio" class="registration-radio" id="test2" name="radio-group">
                                    <label for="test2" class="no-mb-label ml-3">Нерезидент РК</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row d-flex flex-row align-items-center">
                            <label for="citizenship" class="col-sm-2 col-form-label required d-flex justify-content-sm-end text-right">Пол*</label>
                            <div class="col-sm-4">
                                <select class="form-select form-control" aria-label="Default select example">
                                    <option selected disabled>Выберите пол</option>
                                    <option value="1">Женский</option>
                                    <option value="2">Мужской</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row d-flex flex-row align-items-center">
                            <label for="citizenship" class="col-sm-2 col-form-label required d-flex justify-content-sm-end text-right">Полная дата рождения*</label>
                            <div class="col-sm-6">
                                <div class="d-flex flex-row">
                                    <select class="form-select form-control col-sm-2" aria-label="Default select example">
                                        <option selected disabled>01</option>
                                        <option value="1">Женский</option>
                                        <option value="2">Мужской</option>
                                    </select>
                                    <select class="form-select form-control col-sm-5 ml-2" aria-label="Default select example">
                                        <option selected disabled>Месяц</option>
                                        <option value="1">Женский</option>
                                        <option value="2">Мужской</option>
                                    </select>
                                    <select class="form-select form-control col-sm-4 ml-2" aria-label="Default select example">
                                        <option selected disabled>Год</option>
                                        <option value="1">Женский</option>
                                        <option value="2">Мужской</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div id="registration-step-2" class="tabcontent registration-tab">
                </div>
                <div id="registration-step-3" class="tabcontent registration-tab">
                </div>
                <div id="registration-step-4" class="tabcontent registration-tab">
                </div>
            </div>
            <div class="d-flex flex-row mt-4">
                <button class=" yellow-border-btn btn-respond">Сохранить</button>
                <a href="/" class="yellow-border-btn btn-respond ml-4 centralize-content-inside remove-underline">Выйти</a>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready (function(e){
            document.getElementById("defaultOpen").click();
        })

        function openTab(evt, cityName) {
            // Declare all variables
            var i, tabcontent, register_tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="register_tablinks" and remove the class "active"
            register_tablinks = document.getElementsByClassName("register_tablinks");
            for (i = 0; i < register_tablinks.length; i++) {
                register_tablinks[i].className = register_tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
@endsection
