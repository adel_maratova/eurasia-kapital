@extends('layouts.mainApp')
@section('content')
    <div class="why-invest">
        <div class="why-invest background row-full"></div>
            <div class="why-invest content">
                <div class="page-title mt-5" id="growth-leaders">Почему нужно инвестировать</div>
                <div class="d-flex flex-row justify-content-between align-items-center mt-5">
                    <div class="white-shadow-box">
                        <b>Доходность по депозитам</b> <br>
                        с 01.04.2020 по 01.04.2021 составила
                    </div>
                    <div class="percent-round">10-12%</div>
                    <div class="double-arrows"><img src="img/icons/next.png"><img src="img/icons/next.png"></div>
                    <div class="d-flex justify-content-end align-items-center">
                        <div class="white-shadow-box ml-5 ">
                            <b>Доходность по депозитам (индекс KASE)</b> <br>
                            с 01.04.2020 по 01.04.2021 выросли на
                        </div>
                        <div class="percent-round percent-yellow-gradient-round" ><div class="d-flex align-items-center">39<div style="font-size:20px " class="ml-1">%</div></div></div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center mt-5">
                    <div class="white-shadow-box why-invest-point-lower-box">
                        <b>Годовая доходность по долларовым<br> депозитам</b> <br>
                        остается по прежнему на уровне
                    </div>
                    <div class="percent-round">1%</div>
                    <div class="double-arrows"><img src="img/icons/next.png"><img src="img/icons/next.png"></div>
                    <div class="d-flex flex-row justify-content-between align-items-center">
                        <div class="white-shadow-box ml-5 why-invest-point-lower-box ">
                            <b>Долларовые облигации и ETF на долларовые облигации имеют доходность</b> <br>
                        </div>
                        <div class="percent-round percent-yellow-gradient-round"><div class="d-flex align-items-center">4 -10<div style="font-size:20px " class="ml-1">%</div></div></div>
                    </div>
                </div>
                <div class="where-data-from mt-10">
                    <div class="par-title">Откуда такие данные?</div>
                    <div class="d-flex flex-row mt-4">
                        <div class="col p-0 mr-2">
                            <div class="d-flex flex-row">
                                <img src="{{asset('img/icons/product-growth.png')}}" alt="">
                                <div class="lh-21 black-16 ml-4 ">На апрель 2021 года годовая инфляция с марта 2020 по март 2021 составляет <b>7%</b>, а продовольственные товары выросли на <b>10,7%</b>. </div>
                            </div>
                            <div class="lh-21 black-16 mt-2">
                                Депозиты (с частичным снятием) в банках дают ставку <b>8%</b> - <b>9%</b>.
                                Таким образом, доход по депозиту перекрывает только инфляцию.
                            </div>
                            <div class="d-flex flex-row mt-6">
                                <img src="{{asset('img/icons/usd-devaluation.png')}}" alt="" class="usd-devaluation-pic">
                                <div class="lh-21 black-16 ml-4">
                                    Доллары обесцениваются в год на <b>3%</b>
                                    <div class="mt-2"> Долларовые депозиты в Казахстане дают ставку <b>1%</b> годовых. Значит, депозиты не покрывают даже долларовую инфляцию.</div>
                                </div>
                                <div></div>
                            </div>
                        </div>
                        <div class="col p-0 ml-5">
                            <div class="d-flex flex-row">
                                <img src="{{asset('img/icons/tg-stock.png')}}" alt="">
                                <div class="lh-21 black-16 ml-4 ">Тенговые акции на бирже KASE (индекс KASE) за первые три месяца 2021 года выросли на <b>16%</b>, что эквивалентно годовой доходности <b>64%</b>. <a href="https://kase.kz/ru/shares/" class="light-blue-link">(https://kase.kz/ru/shares/)</a></div>
                            </div>
                            <div class="lh-21 black-16 mt-2">
                                Корпоративные тенговые облигации на бирже KASE торгуются по доходности от <b>8%</b> до <b>20%</b> годовых. (https://kase.kz/ru/bonds/)
                            </div>
                            <div class="d-flex flex-row mt-6">
                                <img src="{{asset('img/icons/usd-stock.png')}}" alt="" class="usd-devaluation-pic">
                                <div class="lh-21 black-16 ml-4">
                                    Долларовые акции (индекс S&P500) за первые три месяца 2021 года выросли на <b>6,8%</b>, что эквивалентно годовой доходности <b>27,2%</b>.
                                </div>
                            </div>
                            <div class="lh-21 black-16 mt-2">
                                Долларовые облигации на KASE торгуются с доходностью до <b>8%</b> годовых. Долларовые ETF и облигации на зарубежных рынках до <b>15%</b> годовых.
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-between mt-3">
                        <div class="dialog-cloud col ml-0 mr-3">Вот так растет инфляция в Казахстане</div>
                        <div class="dialog-cloud col ml-2">Инвестиции гораздо выгоднее депозитов!</div>
                    </div>
                </div>
            </div>
        </div>
    <div class="why-enterpreneurs-invest mt-9">
        <div class="black-16">
            Инвестиции помогают накопить на обучение, путешествия, новую машину, дом в Испании, пенсию и на что угодно ещё. Чем раньше вы начнёте инвестировать, тем быстрее достигнете своей финансовой цели.<br><br>
            Инвестировать можно не только для того, чтобы получить доход.
        </div>
        <div class="par-title mt-5">
            Почему нужно инвестировать предпринимателям?
        </div>
        <div class="black-16 mt-4">
            Когда предприниматель начинает новый проект, он должен сравнить следующее:
        </div>
        <div class="d-flex flex-row mt-5">
            <div class="col-sm-1"><img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/first-step-point-pic.png')}}"></div>
            <div class="col-sm-5">
                <div class="step-title">Что выгоднее?</div>
                <div class="black-16 mt-2">
                    Положить деньги, которые он хотел вложить в проект на депозит в банк
                </div>
                <div class="light-gray-700-36">или</div>
                <div class="black-16 mt-2">
                    вложить в проект и получить чистую прибыль <br> (за минусом расходов)?
                </div>
            </div>
            <div class="col-sm-6"><img src="{{asset('img/icons/weights.png')}}" class="weights-pic" alt=""></div>
        </div>
        <div class="d-flex flex-row mt-6">
            <div class="col-sm-1"><img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/second-step-point-pic.png')}}"></div>
            <div class="col-sm-9">
                <div class="black-16">
                    <b>Если предполагаемая чистая прибыль выше дохода по депозиту</b>, то предприниматель должен
                    сумму чистой прибыли сравнить с доходом, который он мог бы получить от инвестиций в ценные бумаги (ETF, акции, облигации).
                </div>
                <div class="black-16 mt-2">
                    Помочь в определении потенциального дохода на вложенные деньги поможет брокер.
                    Доход зависит от суммы, срока вложения и аппетита к риску у предпринимателя.
                </div>
            </div>
        </div>
    </div>
    <div class="light-blue why-enterpreneurs-invest-description">
        <div class=" light-blue why-enterpreneurs-invest-description background row-full"></div>
        <div class=" why-enterpreneurs-invest-description content">
            Этот подход можно применять не только к коммерческим проектам, но к идеям предпринимателей или обычных граждан по получению дохода от покупки недвижимости с дальнейшей сдачей ее в аренду.<br><br>
            Необходимо сравнить стоимость аренды за год (за минусом расходов на ремонт и другие расходы) с доходом от инвестирования в ценные бумаги. Если хотите небольшой бизнес в виде магазина продуктов
            «у дома», небольшой донерной и т.д., то всегда сравнивайте предполагаемую чистую прибыль с прибылью от инвестирования в ценные бумаги. Это поможет выявить эффективность вашего проекта.
        </div>
    </div>
    <div class="where-to-start-section">
        <div class="par-title mt-4">С чего начать?</div>
        <div class="black-16 mt-4">
            Если Вы решили начать торговать на бирже, то необходимо понять сразу, что инвестировать нужно только свободные средства.
            Ни в коем случае не заемные. Перед тем как использовать новый гаджет вы стараетесь сначала прочитать инструкцию пользователя.
            Поэтому рекомендуем Вам сначала почитать про фондовый рынок популярные книжки. Например Уильям Дж. О`Нил «Как делать деньги на фондовом рынке».
            Далее советую поговорить с брокерами, получить их консультации. Определиться с суммой инвестирования и со сроком инвестирования.
        </div>
        <div class="mt-4">
            <b>Можно выделить несколько этапов:</b>
        </div>
        <div class="d-flex flex-row mt-4">
            <div class="col-sm">
                <div class="d-flex flex-row">
                    <img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/first-step-point-pic.png')}}">
                    <div class="ml-2">
                        Выберите брокера по принципу «семь раз отмерь один раз отрежь».
                        Пообщайтесь с консультантами-брокерами и вы поймете с кем вам комфортно будет вести дела в дальнейшем.
                    </div>
                </div>
                <div class="d-flex flex-row mt-3">
                    <img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/second-step-point-pic.png')}}">
                    <div class="ml-2">
                        Определившись с брокером, приготовьте копию удостоверения личности.
                        Вас попросят заполнить учетную карточку, анкету. Вам приготовят приказ на открытие счета,
                        брокерский договор и возможно еще другие документы.
                    </div>
                </div>
                <div class="d-flex flex-row mt-3">
                    <img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/third-step-point-pic.png')}}">
                    <div class="ml-2">
                        Далее необходимо будет пополнить свой брокерский счет по выданным вам реквизитам.
                        Всегда отслеживайте в режиме реального времени изменения котировок и цен по финансовым
                        инструментам. Обычно это можно делать с помощью торговых платформ.
                    </div>
                </div>
                <div class="d-flex flex-row mt-3">
                    <img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/fourth-step-point-pic.png')}}">
                    <div class="ml-2">
                        Советуем начинать торговать на казахстанском рынке и далее переходить
                        на зарубежные рынки. Смотрите статистику цен финансового инструмента который вам
                        понравился, на что он реагирует (новости, сезонные факторы и т.д.).
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="d-flex flex-row">
                    <img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/fifth-step-point-pic.png')}}">
                    <div class="ml-2">
                        Ищите оптимальный момент для покупки или продажи ценной бумаги. Это не всегда так просто,
                        нужно быть внимательным ко всем мелочам. Не забывайте консультироваться с брокерами или
                        трейдерами, так как трейдеры обладая техническим анализом, могут подсказать вам точку входа
                        и выхода из бумаги.
                    </div>
                </div>
                <div class="d-flex flex-row mt-3">
                    <img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/sixth-step-point-pic.png')}}">
                    <div class="ml-2">
                        Вскоре у Вас появится своя четкая торговая стратегия, и старайтесь всегда ее придерживаться.
                    </div>
                </div>
                <div class="d-flex flex-row mt-3">
                    <img class="why-enterpreneurs-invest-step" src="{{asset('img/icons/steps/seventh-step-point-pic.png')}}">
                    <div class="ml-2">
                        Контролируйте свои эмоции, когда у Вас будет определенный опыт, Вам будет легче с этим
                        справиться, а сейчас, на начальном этапе, нужно все время об этом помнить и контролировать
                        свое состояние. «Не храните все яйца в одной корзине» - диверсифицируйте портфель.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="open-account-request-form row-full d-flex justify-content-center light-blue">
        <div class="">
            <div class="page-title mt-5 d-flex justify-content-sm-center">Определились с выбором?<br>
                Откроте счет и покупайте
            </div>
            <div class="small-title mt-2 d-flex justify-content-center">
                Открытие брокерского счета (шаг 1 из 4)
            </div>
            <div>
                <input class="mt-5 details-txt-input" type="text" placeholder="Фамилия, имя и отчество">
            </div>
            <div>
                <input class="mt-3 details-txt-input" type="text" placeholder="Мобильный телефон">
            </div>
            <div class="d-flex justify-content-center">
                <button class="mt-4 btn btn-rounded btn-yellow d-flex justify-content-center">Открыть счет</button>
            </div>
            <div class="mt-4 d-flex justify-content-center">
                Или напишите нам на WhatsApp  <img src="img/contact/whatsapp-logo.png" class="mr-2 ml-2"> +7 (777) 033 19 76
            </div>
        </div>
    </div>
@endsection
