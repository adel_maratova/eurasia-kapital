@extends('layouts.mainApp')
@section('content')
    <div class="page-title mt-5" id="growth-leaders">Обучение</div>
    <div class="education-tabs-section mt-4">
        <div class="tab">
            <button class="tablinks" onclick="openTab(event, 'articles')" id="defaultOpen">Статьи</button>
            <button class="tablinks" onclick="openTab(event, 'literature')">Литература</button>
            <button class="tablinks" onclick="openTab(event, 'video')">Видео</button>
        </div>

        <!-- Tab content -->
        <div id="articles" class="tabcontent">
            <div class="d-flex flex-row">
                <div class="col-sm p-0">
                    <div class="education-info-preview first">
                        <div class="news-block-title mt-2">Обзор перед IPO ZoomInfo Technologies (ZI): платформа для сокращения цикла продаж</div>
                        <div class="news-block-description mt-3">
                            Можно заработать: <br>
                            До 61%  за 11 месяцев
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                </div>
                <div class="col-sm p-0 ml-8">
                    <div class="education-info-preview first">
                        <div class="news-block-title mt-2">Обзор перед IPO ZoomInfo Technologies (ZI): платформа для сокращения цикла продаж</div>
                        <div class="news-block-description mt-3">
                            Можно заработать: <br>
                            До 61%  за 11 месяцев
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                    <div class="education-info-preview mt-3-5">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                </div>
            </div>
            <div class="blue1 link-more-details d-flex justify-content-center ">Показать еще</div>
        </div>

        <div id="literature" class="tabcontent">
            <table id="literature-table" class="table table-literature">
                <thead>
                <tr>
                    <th class="">Сортировка по алфавиту</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="literature-title">1. Презентация по работе рынка ценных бумаг: часть 1</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">2. Презентация по работе рынка ценных бумаг: часть 2</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">3. Презентация по работе рынка ценных бумаг: часть 3</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">4. Как получить доход на временно свободные тенге и доллары</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">5. Лучше чем валютный депозит</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">6. Особенности привлечения инвестиций через облигации</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>


                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">Телеграм-канал с книгами об инвестициях</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">Аудиокнига об инвестициях “Тут название книги какое-то, возможно длинное, поэтому две строки”</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">Телеграм-канал с книгами об инвестициях</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class=" d-flex flex-row justify-content-between">
                        <div class="literature-title">Аудиокнига об инвестициях “Тут название книги какое-то, возможно длинное, поэтому две строки”</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="d-flex flex-row justify-content-between">
                        <div class="literature-title">Аудиокнига об инвестициях “Тут название книги какое-то, возможно длинное, поэтому две строки”</div>
                        <div>
                            <a href="#"><img src="{{asset('img/icons/cloud-download.png')}}" alt=""></a>
                            <a href="#" class="ml-2"><img src="{{asset('img/icons/view-only.png')}}" alt=""></a>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="blue1 link-more-details d-flex justify-content-center ">Показать еще</div>
        </div>
        <div id="video" class="tabcontent d-flex flex-row mt-7">
            <div class="d-flex flex-column">
                <div class="align-items-center">
                    <iframe width="570" height="262" src="https://www.youtube.com/embed/j_rdnV9-Efk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                    <div class="news-block-date-source mt-4">
                        9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                    </div>
                </div>
                <div class="mt-9">
                    <iframe width="570" height="262" src="https://www.youtube.com/embed/j_rdnV9-Efk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="piece-of-news">
                    <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                    <div class="news-block-date-source mt-4">
                        9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column ml-8">
                <div class="">
                    <iframe width="570" height="262" src="https://www.youtube.com/embed/j_rdnV9-Efk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="piece-of-news">
                    <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                    <div class="news-block-date-source mt-4">
                        9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                    </div>
                </div>
                <div class="mt-9">
                    <iframe width="570" height="262" src="https://www.youtube.com/embed/j_rdnV9-Efk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="piece-of-news">
                    <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                    <div class="news-block-date-source mt-4">
                        9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready (function(e){
            document.getElementById("defaultOpen").click();
        })

        function openTab(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        $(document).ready(function() {
            $('#literature-table').DataTable({
                'searching': false,
                'ordering': true,
                "lengthChange": false,
                'info':false,
                'pagingType':'full_numbers',
                'language': {
                    'paginate': {
                        'first': '<<',
                        'previous': '<',
                        'next': '>',
                        'last': '>>',
                    }
                },
                'stripped':false,
            });
        } );
    </script>
@endsection
