@extends('layouts.mainApp')
@section('content')
    <div class="contacts-list">
        <div class="contacts-list background"></div>
        <div class="contacts-list content">
            <div class="page-title mt-4 o-90p">Контакты</div>
            <div class="d-flex flex-row">
                <div class="col-sm-3 mt-6 p-0">
                    <div class="contacts-category">Адрес</div>
                    <div class="contacts-data">
                        Желтоксан, 59<br>
                        050004, Алматы<br>
                        Казахстан
                    </div>
                </div>
                <div class="col-sm-4 mt-6">
                    <div class="contacts-category">Телефоны</div>
                    <div class="contacts-data">
                        Тел.: +7 (727) 333 40 20<br>
                        WhatsApp: +7 777 033 19 76<br>
                        Факс: +7 (727) 333 40 21<br>
                    </div>
                </div>
                <div class="col-sm-5 mt-6">
                    <div class="contacts-category">График работы</div>
                    <div class="contacts-data">
                        9:00-18:00 (будние дни)
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="feedback-form-section row-full d-flex justify-content-center light-blue">
        <div class="container">
            <div class="page-title mt-4 d-flex justify-content-sm-start">Форма обратной связи</div>
            <div><input class="mt-3 details-txt-input" type="text" placeholder="Фамилия, имя"></div>
            <div><input class="mt-3 details-txt-input" type="text" placeholder="Мобильный телефон"></div>
            <div><input class="mt-3 details-txt-input" type="text" placeholder="Почта"></div>
            <div class="form-group">
                <textarea class=" mt-3 details-textarea-input" id="exampleFormControlTextarea1" placeholder="Ваше сообщение" rows="3"></textarea>
            </div>
            <div class="d-flex justify-content-start">
                <button class="mt-4 btn btn-rounded btn-yellow d-flex justify-content-start">Открыть счет</button>
            </div>
            <div class="mt-4-5 d-flex justify-content-start">Или напишите нам на WhatsApp  <img src="img/contact/whatsapp-logo.png" class="mr-2 ml-2"> +7 (777) 033 19 76</div>
        </div>
    </div>
@endsection
