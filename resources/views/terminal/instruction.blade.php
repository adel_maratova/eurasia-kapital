@extends('layouts.mainApp')
@section('content')
    <div class="page-title mt-5" id="growth-leaders">Инструкция по работе</div>
    <div class="black-16 mt-3" style="height: 584px">Тут инструкция по работе терминала<br><br>

        Tradernet.kz – это торговая платформа, которая предоставляет доступ на рынки Казахстана, России, Америки и
        Европы. Открыв счет через Freedom 24, вы можете торговать на Казахстанской бирже KASE через платформу
        tradernet.kz, логин и пароль такой же, как и на F24. В разделе "Профиль" вы найдете все купленные вами акции,
        там же можно смотреть детализацию по счету и архив совершенных сделок.<br><br>

        Через Tradernet вы также можете совершать такие операции, как конвертация валюты,
        изменение личных данных, смена номера или email, покупка ЗПИФ.
    </div>
@endsection
