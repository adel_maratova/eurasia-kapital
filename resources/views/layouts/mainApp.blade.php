<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
        <script type = "text/javascript" language = "javascript">
            $(window).ready(function() {

                $('li.dropdown').mouseenter(

                    function () {

                        $('div.dropdown-menu-background').css("display","block");
                    },
                );

                $('li.dropdown').mouseleave(

                    function () {

                        $('div.dropdown-menu-background').css("display","none");
                    },
                );

            });
        </script>
        @yield('scripts')
    </head>
    <body>
        <div id="app" class="p-0">
            <div class="container-fluid p-0">
                <div class="container-fluid p-0">
                    <header>
                        <div class="upper-header">
                            <div class="row col-md-8 offset-md-4 upper-header-background pr-0"></div>
                            <div class="row col-md-4 offset-md-6 upper-header-content">
                                <nav class="navbar navbar-expand-md navbar-light upper-header-nav">
                                    <ul class="navbar-nav d-inline-flex">
                                        <li class="nav-link upper-header-item"><img src="img/contact/whatsapp-logo.png" class="whatsapp-logo"><a href="https://wa.me/87770331976" class="ml-2">+7 (777) 033 19 76</a></li>
                                        <li class="nav-link upper-header-item"><img src="img/contact/phone-icon.png" class="contact-icon"><a href="tel:87273334020" class="ml-2">+7 (727) 333 40 20</a></li>
                                        <li class="nav-link upper-header-item lang  lang-drpdwn" id="dropdown">
                                            <a href="#" class="dropbtn-lang">РУС<img src="img/buttons/arrow-down.png" class="ml-1"> </a>
                                            <div class="dropdown-content-lang">
                                                <a href="#rus">РУС</a>
                                                <a href="#kaz">ҚАЗ</a>
                                                <a href="#eng">ENG</a>
                                            </div>
                                        </li>
                                        <li class="nav-link upper-header-item login-register"> <a href="/login">Войти/Открыть счет</a><img src="img/buttons/login-icon.png" class="ml-2"></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <nav class="navbar navbar-expand-md navbar-light align-items-end lower-header-nav pl-0">
                                <a href="/" class="navbar-brand main-page-logo">
                                    <img src="img/logotype/main-page-logo.png" alt="logotype">
                                </a>
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="navbarContent">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse nav-menu-main-page ml-3" id="navbarContent">
                                    <ul class="navbar-nav d-inline-flex">
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="/catalog" class="lower-header-item catalog-category">Каталог</a>
                                            <div class="dropdown-content">
                                                <a href="/catalog">Все</a>
                                                {{--<a href="/etf">ETF</a>--}}
                                                <a href="/stocks">Акции</a>
                                                <a href="/bonds">Облигации</a>
                                                {{--<a href="/indexes">Индексы</a>--}}
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="" class="lower-header-item">Что купить</a>
                                            <div class="dropdown-content">
                                                <a href="#growth-leaders">Лидеры роста</a>
                                                <a href="#ups-downs">Взлеты и падения</a>
                                                <a href="#popular">Популярные</a>
                                                <a href="#recommendations">Рекомендации</a>
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="/wheretostart" class="lower-header-item">С чего начать</a>
                                            <div class="dropdown-content">
                                                <a href="/whytoinvest">Почему нужно инвестировать</a>
                                                <a href="/how-to-top-up">Как пополнить счет</a>
                                                <a href="/chose-securities">Как выбирать ценные бумаги</a>
                                                <a href="/education">Обучение</a>
                                                <a href="/about-market">О рынке</a>
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="/services" class="lower-header-item">Услуги</a>
                                            <div class="dropdown-content">
                                                <a href="/brokerage-services">Брокерские услуги</a>
                                                <a href="/management-services">Услуги по управлению</a>
                                                <a href="/issue-securities">Выпуск ценных бумаг</a>
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="#" class="lower-header-item">Новости и аналитика</a>
                                            <div class="dropdown-content">
                                                <a href="/news">Новости</a>
                                                <a href="/analytics">Аналитика</a>
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="#" class="lower-header-item">Терминал</a>
                                            <div class="dropdown-content">
                                                <a href="/terminal-opportunities">Возможности</a>
                                                <a href="/terminal-instruction">Инструкция по работе</a>
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="/tariffs" class="lower-header-item">Тарифы</a>
                                            <div class="dropdown-content">
                                                <a href="/tariffs">Виды тарифов</a>
                                                <a href="/contracts-forms">Договор и анкеты</a>
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap dropdown">
                                            <a href="#" class="lower-header-item">О компании</a>
                                            <div class="dropdown-content">
                                                <a href="/history">История</a>
                                                <a href="/structure">Структура</a>
                                                <a href="/team">Команда</a>
                                                <a href="/benefits">Наши преимущества</a>
                                                <a href="/reports">Отчетность и документы</a>
                                                <a href="/bank-details">Реквизиты</a>
                                                <a href="/careers">Вакансии</a>
                                            </div>
                                        </li>
                                        <li class="nav-link text-nowrap">
                                            <a href="/contacts" class="lower-header-item">Контакты</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="dropdown-menu-background"></div>
                    </header>
                </div>
                <div class="container p-0">

                    @yield('content')
                </div>
                <div class="container">
                    <div class="row-full upper-footer-background"></div>
                    <div class="row justify-content-between upper-footer">
                        <div class="col-md-4 row align-items-center">
                            <img src="img/logotype/logo-rus.png">
                        </div>
                        <div class="col-md-3 row justify-content-end align-items-center footer-main-contact">
                                <img src="img/contact/phone-icon.png" class="contact-icon">
                                <a href="tel:87273334020" class="footer-main-tel ml-2">+7 (727) 333 40 20</a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="lower-footer-background row-full" ></div>
                    <div class="row lower-footer-content">
                        <div class="col">
                            <a class="row footer-nav-category footer-heading">Что купить</a>
                            <a class="row footer-nav-category">С чего начинать</a>
                            <a class="row footer-nav-category">Терминал</a>
                            <a class="row footer-nav-category">Обучение</a>
                            <a class="row footer-nav-category">Тарифы</a>
                            <a href="/login" class="row login-footer align-items-center mt-5" >Личный кабинет<img src="img/buttons/login-icon.png" class="ml-2"></a>
                        </div>
                        <div class="col">
                            <a class="row footer-nav-category footer-heading">Каталог</a>
                            <a class="row footer-nav-item">Акции</a>
                            <a class="row footer-nav-item">Облигации</a>
                            <a class="row footer-nav-item">Фонды</a>
                        </div>
                        <div class="col">
                            <a class="row footer-nav-category footer-heading">Услуги</a>
                            <a class="row footer-nav-item">Услуги по управлению</a>
                            <a class="row footer-nav-item">Выпуск ценных бумаг</a>
                        </div>
                        <div class="col">
                            <a class="row footer-nav-category footer-heading">Новости и аналитика</a>
                            <a class="row footer-nav-item">Новости</a>
                            <a class="row footer-nav-item">Аналитика</a>
                        </div>
                        <div class="col">
                            <a class="row footer-nav-category footer-heading">О компании</a>
                            <a class="row footer-nav-item">История</a>
                            <a class="row footer-nav-item">Структура</a>
                            <a class="row footer-nav-item">Команда</a>
                            <a class="row footer-nav-item">Преимущества</a>
                            <a class="row footer-nav-item">Отчетность и документы</a>
                            <a class="row footer-nav-item">Реквизиты</a>
                            <a class="row footer-nav-item">Вакансии</a>
                        </div>
                        <div class="col">
                            <a class="row footer-nav-category footer-heading">Контакты</a>
                            <a class="row align-items-center"><img src="img/signs/location.png" class="location-sign mr-3"> <div class="footer-contact">г. Алматы, Желтоксан, 59</div></a>
                            <a class="row align-items-center"><img src="img/contact/whatsapp-logo.png" class="whatsapp-footer mr-2"><div class="footer-contact">+7 777 033 19 76</div></a>
                            <a class="row ml-2"><div class="footer-contact">+7 (727) 333 40 21 (факс)</div></a>
                            <a class="row align-items-center"><img src="img/contact/email.png" class="email-icon mr-2"><div class="footer-contact">info@ecap.kz</div></a>
                            <a class="row align-items-center justify-content-end mt-5 mr-2"><img src="img/social/facebook.png" class="facebook-icon"><img src="img/social/instagram.png" class="facebook-icon ml-3"></a>
                            <div class="row justify-content-end mt-3 rights-reserved">АО «Евразийский Капитал».<br>Все права защищены.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
