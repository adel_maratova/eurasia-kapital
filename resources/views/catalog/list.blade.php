@extends('layouts.mainApp')
@section('content')
    <div class="page-title mt-5">Каталог</div>
    <div class="search-line">
        <img class="img-magnifier" src="{{asset('img/icons/magnifier.png')}}" alt="">
        <input type="text" class="search-line-input" placeholder="Введите название или тикер">
    </div>
    <div class="light-blue tbl-key-indicators mb-2 ">
        <div class="d-flex justify-content-between p-2 ">
            <div class="blue-14">Ключевые показатели</div>
            <div class="light-gray-12">Торги на бирже KASE открываются через 3 ч 39 мин</div>
        </div>
        <div class="row p-2">
            <div class="col white-border-right">
                <div class="black-16 thin-white-underline p-1">S&P 500</div>
                <div class="black-16 font-weight-bold p-1">4077.91</div>
                <div class="d-flex justify-content-between black-12">+58.040 (+1.44%) <img src="img/icons/up.png" class="arrow-up"></div>
            </div>
            <div class="col">
                <div class="black-16 thin-white-underline p-1">Nasdaq</div>
                <div class="black-16 font-weight-bold p-1">6737,91</div>
                <div class="d-flex justify-content-between black-12">+23,670 (+0,35%)<img src="img/icons/up.png" class="arrow-up"></div>
            </div>
            <div class="col">
                <div class="black-16 thin-white-underline p-1">FTSE 100</div>
                <div class="black-16 font-weight-bold p-1">6737,91</div>
                <div class="d-flex justify-content-between black-12">-23,670 (-0,35%)<img src="img/icons/red-down.png" class="arrow-up"></div>
            </div>
            <div class="col">
                <div class="black-16 thin-white-underline p-1">Nasdaq</div>
                <div class="black-16 font-weight-bold p-1">6737,91</div>
                <div class="d-flex justify-content-between black-12">+23,670 (+0,35%)<img src="img/icons/up.png" class="arrow-up"></div>
            </div>
            <div class="col">
                <div class="black-16 thin-white-underline p-1">FTSE 100</div>
                <div class="black-16 font-weight-bold p-1">6737,91</div>
                <div class="d-flex justify-content-between black-12">-23,670 (-0,35%)<img src="img/icons/red-down.png" class="arrow-up"></div>
            </div>
            <div class="col">
                <div class="black-16 thin-white-underline p-1">FTSE 100</div>
                <div class="black-16 font-weight-bold p-1">6737,91</div>
                <div class="d-flex justify-content-between black-12">-23,670 (-0,35%)<img src="img/icons/red-down.png" class="arrow-up"></div>
            </div>
        </div>
    </div>
    <div class="catalog-etf mt-5">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="page-title">ETF</div>
            <div class="see-more">Смотреть все</div>
        </div>
        <div class="catalog-row d-flex flex-row mt-4">
            <div class="col white-shadow-box opportunities">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class="col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ETF</div><div class="kaz-tel" ></div></div>
                <div class="company-title">FXKZ ETF</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ОБЛИГАЦИИ</div><div class="es"></div></div>
                <div class="company-title">Электроцит-
                    Стройсистема выпуск 2</div>
                <div class="card-legend mt-2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
        </div>
    </div>
    <div class="catalog-etf mt-5">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="page-title">Акции</div>
            <div class="see-more">Смотреть все</div>
        </div>
        <div class="catalog-row d-flex flex-row mt-4">
            <div class="col white-shadow-box opportunities">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class="col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ETF</div><div class="kaz-tel" ></div></div>
                <div class="company-title">FXKZ ETF</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ОБЛИГАЦИИ</div><div class="es"></div></div>
                <div class="company-title">Электроцит-
                    Стройсистема выпуск 2</div>
                <div class="card-legend mt-2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
        </div>
    </div>
    <div class="catalog-etf mt-5">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="page-title">Облигации</div>
            <div class="see-more">Смотреть все</div>
        </div>
        <div class="catalog-row d-flex flex-row mt-4">
            <div class="col white-shadow-box opportunities">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class="col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ETF</div><div class="kaz-tel" ></div></div>
                <div class="company-title">FXKZ ETF</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ОБЛИГАЦИИ</div><div class="es"></div></div>
                <div class="company-title">Электроцит-
                    Стройсистема выпуск 2</div>
                <div class="card-legend mt-2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
        </div>
    </div>
    <div class="catalog-etf mt-5 mb-5">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="page-title">Индексы и другие фининструменты</div>
            <div class="see-more">Смотреть все</div>
        </div>
        <div class="catalog-row d-flex flex-row mt-4">
            <div class="col white-shadow-box opportunities">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class="col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">Акции</div><div class="kaz-tel" ></div></div>
                <div class="company-title">Казахтелеком</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ETF</div><div class="kaz-tel" ></div></div>
                <div class="company-title">FXKZ ETF</div>
                <div class="card-legend mt-2">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
            <div class=" col white-shadow-box opportunities ml-5">
                <div class="d-flex justify-content-between"><div class="item-title">ОБЛИГАЦИИ</div><div class="es"></div></div>
                <div class="company-title">Электроцит-
                    Стройсистема выпуск 2</div>
                <div class="card-legend mt-2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ">27 500 ₸</div>
                <div class="d-inline-flex align-items-center mt-3">
                    <img src="img/icons/up.png" class="arrow-up">
                    <div class="card-legend ml-2">34%</div>
                    <img src="img/icons/line-growth.png" class="ml-3 line-growth">
                    <div class="gray-info-icon ml-3"></div>
                </div>
                <button class="yellow-border-btn mt-3">Купить</button>
            </div>
        </div>
    </div>
    <div class="might-interest">
        <div class="might-interest background row-full"></div>
        <div class="might-interest content">
            <div class="d-flex">
                <div class="page-title mt-6">Вам можеть быть интересно</div>
            </div>
            <div class="d-flex flex-row mt-4-5">
                <div class="col-sm-8 p-0">
                    <div class="piece-of-news">
                        <div class="news-block-title mt-2">Обзор перед IPO ZoomInfo Technologies (ZI): платформа для сокращения цикла продаж</div>
                        <div class="news-block-description mt-3">
                            Можно заработать: <br>
                            До 61%  за 11 месяцев
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="piece-of-news">
                        <div class="news-block-title mt-2">Предварительный обзор перед IPO Inari Medical, Inc (NARI): новые методы лечения венозных заболеваний</div>
                        <div class="news-block-date-source mt-4">
                            9 марта 2021   |    ZoomInfo Technologies Inc (ZI)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="open-account-request-form row-full d-flex justify-content-center light-blue">
        <div class="">
            <div class="page-title mt-5 d-flex justify-content-sm-center">Определились с выбором?<br>
                Откроте счет и покупайте</div>
            <div class="small-title mt-2 d-flex justify-content-center">Открытие брокерского счета (шаг 1 из 4)</div>
            <div><input class="mt-5 details-txt-input" type="text" placeholder="Фамилия, имя и отчество"></div>
            <div><input class="mt-3 details-txt-input" type="text" placeholder="Мобильный телефон"></div>
            <div class="d-flex justify-content-center">
                <button class="mt-4 btn btn-rounded btn-yellow d-flex justify-content-center">Открыть счет</button>
            </div>
            <div class="mt-4 d-flex justify-content-center">Или напишите нам на WhatsApp  <img src="img/contact/whatsapp-logo.png" class="mr-2 ml-2"> +7 (777) 033 19 76</div>
        </div>
    </div>
@endsection
