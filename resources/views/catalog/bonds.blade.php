@extends('layouts.mainApp')
@section('content')
    <div class="page-title mt-5">Каталог - Облигации</div>
    <div class="search-line">
        <img class="img-magnifier" src="{{asset('img/icons/magnifier.png')}}" alt="">
        <input type="text" class="search-line-input" placeholder="Введите название или тикер">
    </div>
    <div class="bonds-table-section">
        <table id="bonds" class="table bonds-table">
            <thead>
            <tr>
                <th class="col-sm-5 bonds-company-column">Сортировка по алфавиту</th>
                <th class="col-sm-2 bonds-profit-column">Доходность</th>
                <th class="col-sm-3 bonds-maturity-column">Дата гашения</th>
                <th class="col-sm-4 bonds-price-column">Цена (с купоном)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком выпуск 1</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="kazahtelecom-logo col-sm-2"></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Казахтелеком</div>
                        <div class="stock-company-abbr">KZTK</div>
                    </div>
                </td>
                <td class="fw-400-s-20">9,35%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">7 апреля 2025 </div>
                    <div class="gray-400-16">через 4 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1000 ₸</td>
            </tr>
            <tr>
                <td class=" d-flex flex-row align-items-center">
                    <div class="col-sm-2"><img src="{{asset('img/company-logos/pfizer-logo.png')}}" alt=""></div>
                    <div class="col d-flex flex-column">
                        <div class="stock-company-title">Pfizer</div>
                        <div class="stock-company-abbr">PFE</div>
                    </div>
                </td>
                <td class="company-data-cell fw-400-s-20">8,12%</td>
                <td class="company-data-cell">
                    <div class="fw-400-s-20">31 октября 2023</div>
                    <div class="gray-400-16">через 2 года</div>
                </td>
                <td class="company-data-cell fw-400-s-20">1379 ₸</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="open-account-request-form row-full d-flex justify-content-center light-blue">
        <div class="">
            <div class="page-title mt-5 d-flex justify-content-sm-center">Определились с выбором?<br>
                Откроте счет и покупайте</div>
            <div class="small-title mt-2 d-flex justify-content-center">Открытие брокерского счета (шаг 1 из 4)</div>
            <div><input class="mt-5 details-txt-input" type="text" placeholder="Фамилия, имя и отчество"></div>
            <div><input class="mt-3 details-txt-input" type="text" placeholder="Мобильный телефон"></div>
            <div class="d-flex justify-content-center">
                <button class="mt-4 btn btn-rounded btn-yellow d-flex justify-content-center">Открыть счет</button>
            </div>
            <div class="mt-4 d-flex justify-content-center">Или напишите нам на WhatsApp  <img src="img/contact/whatsapp-logo.png" class="mr-2 ml-2"> +7 (777) 033 19 76</div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#bonds').DataTable({
                'searching': false,
                'ordering': true,
                "lengthChange": false,
                'info':false,
                'pagingType':'full_numbers',
                'language': {
                    'paginate': {
                        'first': '<<',
                        'previous': '<',
                        'next': '>',
                        'last': '>>',
                    }
                },
                'stripped':false,
            });
        });
    </script>

@endsection
