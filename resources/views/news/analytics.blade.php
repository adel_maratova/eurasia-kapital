@extends('layouts.mainApp')
@section('content')
    <div class="page-title mt-5" >Аналитика</div>
    <div class="d-flex flex-row">
        <div class="col-sm-8 p-0">
            <div class="education-tabs-section mt-4">
                <div class="tab">
                    <button class="news_tablinks" onclick="openTab(event, 'kaz-market')" id="defaultOpen">Казахстанский рынок</button>
                    <button class="news_tablinks" onclick="openTab(event, 'international-market')">Международный рынок</button>
                    <button class="news_tablinks" onclick="openTab(event, 'company-news')">Новости компании</button>
                </div>

                <!-- Tab content -->
                <div id="kaz-market" class="tabcontent">
                    <div class="mt-3">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="blue1 link-more-details d-flex justify-content-center mt-9">Показать еще</div>
                </div>

                <div id="international-market" class="tabcontent">
                    <div class="mt-3">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="blue1 link-more-details d-flex justify-content-center mt-9">Показать еще</div>
                </div>
                <div id="company-news" class="tabcontent">
                    <div class="mt-3">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="mt-7">
                        <div class="small-title">Льготное кредитование для бизнеса</div>
                        <div class="black-16 mt-2">Повседневная практика показывает, что постоянное информационно-пропагандистское
                            обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации
                            дальнейших направлений развития...
                        </div>
                        <div class="news-block-date-source mt-3">
                            9 марта 2021
                        </div>
                    </div>
                    <div class="blue1 link-more-details d-flex justify-content-center mt-9">Показать еще</div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="d-flex justify-content-center">
                <div class="news-key-indicators-box">
                    <div class="blue-14">Ключевые показатели</div>
                    <div class="row mt-3">
                        <div class="col white-border-right">
                            <div class="black-16 thin-white-underline p-1">S&P 500</div>
                            <div class="black-16 font-weight-bold p-1">4077.91</div>
                            <div class="d-flex justify-content-between black-12">+58.040 (+1.44%) <img src="img/icons/up.png" class="arrow-up"></div>
                        </div>
                        <div class="col">
                            <div class="black-16 thin-white-underline p-1">Nasdaq</div>
                            <div class="black-16 font-weight-bold p-1">6737,91</div>
                            <div class="d-flex justify-content-between black-12">+23,670 (+0,35%)<img src="img/icons/up.png" class="arrow-up"></div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col white-border-right">
                            <div class="black-16 thin-white-underline p-1">FTSE 100</div>
                            <div class="black-16 font-weight-bold p-1">6737,91</div>
                            <div class="d-flex justify-content-between black-12">-23,670 (-0,35%)<img src="img/icons/red-down.png" class="arrow-up"></div>
                        </div>
                        <div class="col">
                            <div class="black-16 thin-white-underline p-1">Nasdaq</div>
                            <div class="black-16 font-weight-bold p-1">6737,91</div>
                            <div class="d-flex justify-content-between black-12">+23,670 (+0,35%)<img src="img/icons/up.png" class="arrow-up"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready (function(e){
            document.getElementById("defaultOpen").click();
        })

        function openTab(evt, cityName) {
            // Declare all variables
            var i, tabcontent, news_tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="news_tablinks" and remove the class "active"
            news_tablinks = document.getElementsByClassName("news_tablinks");
            for (i = 0; i < news_tablinks.length; i++) {
                news_tablinks[i].className = news_tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        $(document).ready(function() {
            $('#literature-table').DataTable({
                'searching': false,
                'ordering': true,
                "lengthChange": false,
                'info':false,
                'pagingType':'full_numbers',
                'language': {
                    'paginate': {
                        'first': '<<',
                        'previous': '<',
                        'next': '>',
                        'last': '>>',
                    }
                },
                'stripped':false,
            });
        } );
    </script>
@endsection
